
public class Player {
	private String name;
	private int win, draw, lose;

	public Player(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getWin() {
		return win;
	}

	public int getDraw() {
		return draw;
	}

	public int getLose() {
		return lose;
	}

	public void setWin() {
		win++;
	}

	public void setDraw() {
		draw++;
	}

	public void setLose() {
		lose++;
	}
}
