import java.util.Scanner;

public class Board {
	private String[][] table = {
			{"-","-","-"},
			{"-","-","-"},
			{"-","-","-"}};		
	private Player x;
	private Player o;
	private Player current;
	private Player winner;
	private int turnCount;
	Scanner kb = new Scanner(System.in);

	public Board(Player x, Player o) {
		this.x = x;
		this.o = o;
		current = x;
		
	}
	public void setTable() {
		for(int i = 0;i < table.length;i++) {
			for(int j = 0;j < table[i].length;j++) {
				table[i][j] = "-";
			}
		}
	}
	public void showBoard() {
		System.out.println("  1" + "  2" + "  3");

		for (int i = 0; i < table.length; i++) {
			System.out.print(i + 1+" ");
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(table[i][j] + "  ");
			}
			System.out.println();
		}
	}
	public void check() {
		while (winner == null) {
			getTurn();
			int row = kb.nextInt();
			int col = kb.nextInt();
			if (OutOfBoard(row, col) == true) {
				System.out.println("Try again!");

			} else {
				if (isFull(row, col)) {
					System.err.println("Again Please\n");

				} else {
					table[row - 1][col - 1] = getCurrent().getName();
					showBoard();
					if (Winner() == false) {
						if (current == x) {
							winner = x;
							System.out.println(getCurrent().getName()+" winner!!"  );
						} else {
							winner = o;
							System.out.println(getCurrent().getName()+" winner!!");
						}
					} else {
						switchPlayer();
						turnCount++;
					}
				}

			}
			if (turnCount > 8) {
				System.out.println("Draw!!");
				break;
			}

		}
		if (getWinner() == x) {
			x.setWin();
			o.setLose();
		} else if (getWinner() == o) {
			o.setWin();
			x.setLose();
		} else {
			o.setDraw();
			x.setDraw();
		}
	}
	public Player getWinner() {
		return winner;
	}
	public boolean Winner() {

		if (table[0][0] != "-" && table[0][0] == table[0][1] && table[0][0] == table[0][2])
			return false;
		if (table[1][0] != "-" && table[1][0] == table[1][1] && table[1][0] == table[1][2])
			return false;
		if (table[2][0] != "-" && table[2][0] == table[2][1] && table[2][0] == table[2][2])
			return false;
		if (table[0][0] != "-" && table[0][0] == table[1][0] && table[0][0] == table[2][0])
			return false;
		if (table[0][1] != "-" && table[0][1] == table[1][1] && table[0][1] == table[2][1])
			return false;
		if (table[0][2] != "-" && table[0][2] == table[1][2] && table[0][2] == table[2][2])
			return false;
		if (table[0][0] != "-" && table[0][0] == table[1][1] && table[0][0] == table[2][2])
			return false;
		if (table[0][2] != "-" && table[0][2] == table[1][1] && table[0][2] == table[2][0])
			return false;

		return true;
	}

	public boolean OutOfBoard(int row, int col) {
		if (row > 3 || col > 3) {
			return true;

		}
		return false;
	}
	public void switchPlayer() {
		if (current == x) {
			current = o;
		} else
			current = x;
	}
	public Player getCurrent() {
		return current;
	}
	private void getTurn() {
		System.out.println(current.getName() + " turn.");
		System.out.println("Please input your row and column: ");

	}
	public boolean isFull(int row, int col) {
		if (table[row - 1][col - 1] == "X" || table[row - 1][col - 1] == "O") {
			return true;
		}
		return false;
	}

    String[][] getTable() {
    }
}
